import json
import re
import requests
from bs4 import BeautifulSoup
from datetime import date
from dateutil import parser

base_url = 'https://www.rozklad.onaft.edu.ua/guest_n.php'


def get_html_parser(url, features='html5lib'):
    response = requests.get(url)
    soup = BeautifulSoup(response.text, features)
    return soup


def get_lesson_name(name_tag, subject_list, added_subjects):
    # all formats met in onaft timetable
    # had to drop last 3 as 'інше'
    # format_map = ['Лекція', 'Семінар', 'Практика', 'Лаб. роб.', 'Дистанційний модуль', 'Консультація', 'Екзамен']

    format_map = ['Лекція', 'Семінар', 'Практика', 'Лаб. роб.']
    name = name_tag['title'].strip()
    if name_tag.br:
        name_tag.br.extract()
    lesson_info = name_tag.get_text().strip().split(" (")
    short_name = lesson_info[0]

    if short_name not in added_subjects:
        subject_list.append({'name': name, 'short_name': short_name})
        added_subjects.add(short_name)

    if len(lesson_info) == 2:
        lesson_format = lesson_info[1]
        lesson_format = lesson_format.strip(')')
        lesson_format = format_map.index(lesson_format) if lesson_format in format_map else len(format_map)
    else:
        lesson_format = len(format_map)
    return name, short_name, lesson_format


def get_teacher_name(teacher_tag, teacher_list, added_teachers):
    teachers = []
    if len(teacher_tag.contents) > 0:
        teacher_full_name = teacher_tag['title'].strip()
        last_name, first_name, middle_name = teacher_full_name.split()
        teacher_dict = {'last_name': last_name, 'first_name': first_name, 'middle_name': middle_name}
        teachers.append(teacher_dict)
        if teacher_full_name not in added_teachers:
            teacher_list.append(teacher_dict)
            added_teachers.add(teacher_full_name)
    return teachers


def get_room_data(room_tag):
    room_data = []
    room_tag = room_tag.strong.get_text().strip()
    if room_tag:
        housing, room_num = room_tag.split('-')
        room_floor = room_num[0]
        room_data.append({'housing': housing, 'room_num': room_num, 'room_floor': room_floor})
    return room_data

# transforming dates like '04.02-25.02' to weeks like '111100000000000000'
def get_weeks(dates, sem_start=date(2019, 2, 4), sem_end=date(2019, 6, 29)):
    dates = dates.strong.get_text().strip()
    sem_start_week, sem_end_week = [d.isocalendar()[1] for d in (sem_start, sem_end)]
    sem_duration = sem_end_week - sem_start_week + 1
    if dates:
        subj_start, subj_end = [parser.parse(d + '.2019', dayfirst=True) for d in dates.strip("*").split('-')]
        subj_start_week, subj_end_week = [d.isocalendar()[1] - sem_start_week for d in (subj_start, subj_end)]

        subj_duration = subj_end_week - subj_start_week + 1
        weeks = ['0'] * sem_duration
        weeks[subj_start_week:subj_end_week] = ['1'] * subj_duration
        return ''.join(weeks)
    return '1' * sem_duration


def format_url(id, view):
    query = '{}&{}'.format("view=" + view, "id=" + id)
    if view == 'g':
        query += '&show_all=1'
    return '{}?{}'.format(base_url, query)


def get_link_list(url):
    soup = get_html_parser(url)
    for a in soup.select('a[href*="&id="]'):
        yield (re.findall(r'\d+', a['href'])[0], a.find(class_="name").get_text().strip())


def write_to_file(data, filename):
    with open(filename, "w", encoding='utf8') as result_file:
        json.dump(data, result_file, indent=2, ensure_ascii=False)


def get_lesson_details(soup, teacher_list, added_teachers, subject_list, added_subjects):
    # get lists for lessons data
    # because for one lesson there are several different subjects
    # divided by dates throughout the semester
    lesson_names = soup.select(".predm")
    dates = soup.select("span.code-text")
    teachers = soup.select(".prp")
    rooms_info = soup.select("a.text-info")

    for l, d, t, r in zip(lesson_names, dates, teachers, rooms_info):
        name_format = get_lesson_name(l, subject_list, added_subjects)

        yield {
            'name': name_format[0],
            'short_name': name_format[1],
            'format': name_format[2],
            'teachers': get_teacher_name(t, teacher_list, added_teachers),
            'room_data': get_room_data(r),
            'weeks': get_weeks(d)
        }


# parsing table with group schedule for semester
def parse_group_timetable(group_name, g_soup, faculty_name, groups):
    lessons_rows = g_soup.tbody.contents
    # group_data = {'faculty_name': faculty_name, 'group_name': group_name}
    group_data = {'group_name': group_name}
    # list with unique teachers ang themes for the group
    teacher_list, subject_list = [], []
    # resulting list with all lessons
    timetable = [],
    added_teachers, added_subjects, lesson_times = set(), set(), set()
    # tracking a weekday
    day = 0
    for lr in lessons_rows:
        if lr.attrs:
            day += 1
            continue
        lesson_tag = lr.find("td", "lesson")
        if lesson_tag is not None:
            lesson_number = lesson_tag.h2.get_text().strip()
            lesson_times.add(lesson_number)
            # tags for different subgroups
            group_lessons = lesson_tag.find_next_siblings("td")
            subgroup = 'а'
            for gl in group_lessons:
                options = gl.strong
                if len(options.contents) > 0:
                    for ld in get_lesson_details(options, teacher_list, added_teachers, subject_list, added_subjects):
                        lesson_details = {'lesson_time': int(lesson_number), 'day': day, 'subgroup': subgroup}
                        lesson_details.update(ld)
                        timetable.append(lesson_details)
                subgroup = chr(ord(subgroup) + 1)
    lesson_times = list(lesson_times)
    lesson_times.sort()
    group_data.update({
        'teachers': teacher_list,
        'themes': subject_list,
        'lessons_times': lesson_times,
        'lessons': timetable
    })
    groups.append(group_data)
    # filename = '{}_{}.json'.format(faculty_name, group_name)
    filename = group_name + '.json'
    write_to_file(group_data, filename)


def get_timetable(file_name):
    faculties = []
    for f_id, f_name in get_link_list(base_url):
        faculty_data = {'faculty_name': f_name}
        groups = []
        f_url = format_url(f_id, view='f')
        for g_id, g_name in get_link_list(f_url):
            g_url = format_url(g_id, view='g')
            g_soup = get_html_parser(g_url)
            parse_group_timetable(g_name, g_soup, f_name, groups)
        faculty_data.update({'groups': groups})
        faculties.append(faculty_data)
    all_faculties = {'faculties': faculties}
    if file_name:
        write_to_file(all_faculties, file_name)
    return all_faculties


if __name__ == '__main__':
    get_timetable('onaft.json')
